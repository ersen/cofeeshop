package main;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import tr.com.shop.coffee.builder.OrderBuilder;
import tr.com.shop.coffee.controller.api.OrderController;
import tr.com.shop.coffee.exception.OrderNotFoundException;
import tr.com.shop.coffee.models.Order;
import tr.com.shop.coffee.service.OrderService;

@RunWith(MockitoJUnitRunner.class)
public class OrderControllerUnitTests {

    @Mock
    private OrderService service;
        
    private MockMvc mockMvc;
    
    private static final Long ID = 1L;
    private static final Long ID2 = 2L;
    
    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(new OrderController(service)).build();
    }
    
    @Test
    public void list_Listed_ShouldReturnResponseStatusOK() throws Exception {
        Order order1 = new OrderBuilder().id(ID).get();
        Order order2 = new OrderBuilder().id(ID2).get();
        
        when(service.listOrders()).thenReturn(Arrays.asList(order1, order2));
        mockMvc.perform(get("/api/order/").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$.", hasSize(2)));
    }
    
    @Test
    public void getOrderItems_OrderNotFound_ShouldReturnResponseStatusNotFound() throws Exception {
        when(service.getOrderById(ID)).thenThrow(new OrderNotFoundException());
        mockMvc.perform(get("/api/order/{id}", ID).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
    }
    
    @Test
    public void getOrderItems_OrderFound_ShouldReturnResponseStatusOK() throws Exception {
        Order order1 = new OrderBuilder().id(ID).get();
        when(service.getOrderById(ID)).thenReturn(order1);
        mockMvc.perform(get("/api/order/{id}", ID).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }
}
