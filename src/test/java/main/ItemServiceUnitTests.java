package main;

import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import tr.com.shop.coffee.builder.AddOnBuilder;
import tr.com.shop.coffee.builder.DrinkBuilder;
import tr.com.shop.coffee.builder.ItemBuilder;
import tr.com.shop.coffee.exception.InvalidRequestParametersException;
import tr.com.shop.coffee.exception.ItemDetailNotFoundException;
import tr.com.shop.coffee.exception.ItemNotFoundException;
import tr.com.shop.coffee.models.Item;
import tr.com.shop.coffee.models.ItemDetail;
import tr.com.shop.coffee.models.Order;
import tr.com.shop.coffee.models.domain.AddOn;
import tr.com.shop.coffee.models.domain.Drink;
import tr.com.shop.coffee.repository.ItemDetailRepository;
import tr.com.shop.coffee.repository.ItemRepository;
import tr.com.shop.coffee.repository.OrderRepository;
import tr.com.shop.coffee.service.ItemService;

@RunWith(MockitoJUnitRunner.class)
public class ItemServiceUnitTests {

    private ItemService service;
    
    @Mock
    private ItemRepository repository;
    
    @Mock
    private ItemDetailRepository itemDetailRepository;

    @Mock
    private OrderRepository orderRepository;

    private static final String NAME = "LATTE";
    private static final int PRICE = 1;
    private static final Long ID = 1L;
    private static final Long ID2 = 2L;
    
    @Before
    public void setup() {
        service = new ItemService(repository, itemDetailRepository, orderRepository);
    }
    
    @Test
    public void addToBasket_Added_DrinkShouldBeIncludedOnCreatedItem() {
        Drink drink = new DrinkBuilder().id(ID).name(NAME).price(PRICE).get();
        
        when(repository.save(isA(Item.class))).thenAnswer(invocation -> (Item) invocation.getArguments()[0]);
        
        Item item = service.addToBasket(drink);
        
        Assert.assertTrue(item.getDrink().getId() == drink.getId());
    }
    
    @Test(expected = ItemNotFoundException.class)
    public void getItemById_NotFound_ShoudThrowException() {
        when(repository.findOneById(ID)).thenReturn(Optional.ofNullable(null));
        service.getItemById(ID);
    }
    
    @Test
    public void getItemById_Found_ShoudReturnTheFound() {
        Item item = new ItemBuilder().id(ID).get();
        when(repository.findOneById(ID)).thenReturn(Optional.of(item));
        Item found = service.getItemById(ID);
        Assert.assertTrue(found.getId() == item.getId());
    }
    
    @Test(expected = InvalidRequestParametersException.class)
    public void addAddOnForItem_ItemOrderedBefore_ShouldThrowException() {
        Item item = new ItemBuilder().id(ID).order(new Order()).get();
        service.addAddOnForItem(item, new AddOn());
    }
    
    @Test(expected = InvalidRequestParametersException.class)
    public void addAddOnForItem_Added_ItemDetailShoulddIncludeTheItemAndAddon() {
        Item item = new ItemBuilder().id(ID).order(new Order()).get();
        AddOn addon = new AddOnBuilder().id(ID).get();
        
        when(itemDetailRepository.save(isA(ItemDetail.class))).thenAnswer(invocation -> (ItemDetail) invocation.getArguments()[0]);
        
        ItemDetail detail = service.addAddOnForItem(item, addon);
        
        Assert.assertTrue(detail.getItem().getId() == item.getId());
        Assert.assertTrue(detail.getAddon().getId() == addon.getId());
    }
    
    @Test(expected = ItemDetailNotFoundException.class)
    public void removeAddOnFromItem_NotFound_ShouldThrowException() {
        when(itemDetailRepository.findOneById(ID)).thenReturn(Optional.ofNullable(null));
        service.removeAddOnFromItem(ID);
    }
    
    @Test
    public void orderBasket_Ordered_OrderItemSizeShouldBeTheNumberOfNonOrderedItems() {
        Item item1 = new ItemBuilder().id(ID).get();
        Item item2 = new ItemBuilder().id(ID2).get();
        
        when(repository.listNonOrderedItems()).thenReturn(Arrays.asList(item1, item2));
        when(orderRepository.save(isA(Order.class))).thenAnswer(invocation -> (Order) invocation.getArguments()[0]);
        
        Order order = service.orderBasket();
        Assert.assertTrue(order.getItems().size() == 2);
    }
    
}
