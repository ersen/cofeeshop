package main;

import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import tr.com.shop.coffee.builder.AddOnBuilder;
import tr.com.shop.coffee.builder.DrinkBuilder;
import tr.com.shop.coffee.exception.InvalidRequestParametersException;
import tr.com.shop.coffee.exception.ProductNotFoundException;
import tr.com.shop.coffee.models.domain.AddOn;
import tr.com.shop.coffee.models.domain.Drink;
import tr.com.shop.coffee.repository.ProductRepository;
import tr.com.shop.coffee.service.ProductService;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceUnitTest {

    @Mock
    private ProductRepository repository;
    
    private ProductService service;
    
    private static final Long ID = 1L;
    
    @Before
    public void setup() {
        this.service = new ProductService(repository);
    }
    
    @Test(expected = InvalidRequestParametersException.class)
    public void getDrinkById_FoundAddon_ShouldThrowException() {
        AddOn addon = new AddOnBuilder().id(ID).get();
        when(repository.findOneById(ID)).thenReturn(Optional.of(addon));
        service.getDrinkById(ID);
    }
    
    @Test(expected = ProductNotFoundException.class)
    public void getDrinkById_NotFound_ShouldThrowException() {
        when(repository.findOneById(ID)).thenReturn(Optional.ofNullable(null));
        service.getDrinkById(ID);
    }
    
    @Test
    public void getDrinkById_Found_ShouldReturnTheProduct() {
        Drink drink = new DrinkBuilder().id(ID).get();
        when(repository.findOneById(ID)).thenReturn(Optional.of(drink));
        Drink found = service.getDrinkById(ID);
        Assert.assertTrue(found.getId() == drink.getId());
    }
    
    @Test(expected = InvalidRequestParametersException.class)
    public void getAddOnById_FoundDrink_ShouldThrowException() {
        Drink drink = new DrinkBuilder().id(ID).get();
        when(repository.findOneById(ID)).thenReturn(Optional.of(drink));
        service.getAddOnById(ID);
    }
    
    @Test(expected = ProductNotFoundException.class)
    public void getAddOnById_NotFound_ShouldThrowException() {
        when(repository.findOneById(ID)).thenReturn(Optional.ofNullable(null));
        service.getAddOnById(ID);
    }
    
    @Test
    public void getAddOnById_Found_ShouldReturnTheProduct() {
        AddOn addon = new AddOnBuilder().id(ID).get();
        when(repository.findOneById(ID)).thenReturn(Optional.of(addon));
        AddOn found = service.getAddOnById(ID);
        Assert.assertTrue(found.getId() == addon.getId());
    }
    
    @Test(expected = ProductNotFoundException.class)
    public void getProductById_NotFound_ShouldThrowException() {
        when(repository.findOneById(ID)).thenReturn(Optional.ofNullable(null));
        service.getProductById(ID);
    }
    
    @Test
    public void getProductById_Found_ShouldReturnTheProduct() {
        AddOn addon = new AddOnBuilder().id(ID).get();
        when(repository.findOneById(ID)).thenReturn(Optional.of(addon));
        AddOn found = (AddOn)service.getProductById(ID);
        Assert.assertTrue(found.getId() == addon.getId());
    }
   
}
