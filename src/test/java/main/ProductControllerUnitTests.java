package main;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import tr.com.shop.coffee.builder.DrinkBuilder;
import tr.com.shop.coffee.controller.api.ProductController;
import tr.com.shop.coffee.enums.ProductType;
import tr.com.shop.coffee.exception.ProductNotFoundException;
import tr.com.shop.coffee.models.domain.Drink;
import tr.com.shop.coffee.service.ItemService;
import tr.com.shop.coffee.service.ProductService;

@RunWith(MockitoJUnitRunner.class)
public class ProductControllerUnitTests {

    @Mock
    private ProductService service;

    @Mock
    private ItemService itemService;

    private MockMvc mockMvc;

    private static final Long ID = 1L;
    private static final Long ID2 = 2L;
    
    private static final ProductType TYPE = ProductType.DRINK;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(new ProductController(service, itemService)).build();
    }
    
    @Test
    public void listByType_listed_ShouldReturnResponseStatusOK() throws Exception {
        Drink drink1 = new DrinkBuilder().id(ID).get();
        Drink drink2 = new DrinkBuilder().id(ID2).get();
        
        when(service.listProductsByType(TYPE)).thenReturn(Arrays.asList(drink1, drink2));
        mockMvc.perform(get("/api/product/{type}", TYPE).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$.", hasSize(2)));
    }
    
    @Test
    public void addToBasket_ItemNotFound_ShouldReturnResponseStatusNotFound() throws Exception {
        when(service.getDrinkById(ID)).thenThrow(new ProductNotFoundException());
        mockMvc.perform(post("/api/product/{productId}/basket", ID).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
    }
    
    @Test
    public void addToBasket_added_ShouldReturnResponseStatusOK() throws Exception {
        Drink drink = new DrinkBuilder().id(ID).get();
        
        when(service.getDrinkById(ID)).thenReturn(drink);
        
        mockMvc.perform(post("/api/product/{productId}/basket", ID).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

}
