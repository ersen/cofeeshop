package main;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import tr.com.shop.coffee.builder.DrinkBuilder;
import tr.com.shop.coffee.builder.ItemBuilder;
import tr.com.shop.coffee.builder.ItemDetailBuilder;
import tr.com.shop.coffee.controller.api.ItemController;
import tr.com.shop.coffee.exception.ItemNotFoundException;
import tr.com.shop.coffee.models.Item;
import tr.com.shop.coffee.models.ItemDetail;
import tr.com.shop.coffee.models.domain.AddOn;
import tr.com.shop.coffee.models.domain.Drink;
import tr.com.shop.coffee.service.ItemService;
import tr.com.shop.coffee.service.ProductService;

@RunWith(MockitoJUnitRunner.class)
public class ItemControllerUnitTests {

    @Mock
    private ItemService service;
    
    @Mock
    private ProductService productService;
    
    private MockMvc mockMvc;
    
    private static final Long ID = 1L;
    private static final Long ID2 = 2L;
    
    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(new ItemController(service, productService)).build();
    }
    
    @Test
    public void list_Listed_ShouldReturnResponseStatusOK() throws Exception {
        Drink drink = new DrinkBuilder().id(ID).get();
        Item item1 = new ItemBuilder().id(ID).drink(drink).get();
        Item item2 = new ItemBuilder().id(ID2).drink(drink).get();
        
        when(service.getNonOrderedBasketItems()).thenReturn(Arrays.asList(item1, item2));
        mockMvc.perform(get("/api/item/unordered").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$.", hasSize(2)));
    }
    
    @Test
    public void listItemAddons_Listed_ShouldReturnResponseStatusOK() throws Exception {
        Item item = new ItemBuilder().id(1L).get();
        
        ItemDetail det1 = new ItemDetailBuilder().id(ID).get();
        ItemDetail det2 = new ItemDetailBuilder().id(ID2).get();
        
        item.setItemDetails(Arrays.asList(det1, det2));
        
        when(service.getItemById(ID)).thenReturn(item);
        mockMvc.perform(get("/api/item/{itemId}/addons", ID).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$.", hasSize(2)));
    }
    
    @Test
    public void listItemAddons_ItemNotFound_ShouldReturnResponseStatusNotFound() throws Exception {
        when(service.getItemById(ID)).thenThrow(new ItemNotFoundException());
        mockMvc.perform(get("/api/item/{itemId}/addons", ID).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
    }
    
    @Test
    public void addAddOnForItem_Added_ShouldReturnResponseOk () throws Exception {
        when(productService.getAddOnById(ID)).thenReturn(new AddOn());
        when(service.getItemById(ID)).thenReturn(new Item());
        mockMvc.perform(post("/api/item/{itemId}/detail/{addOnId}", ID, ID).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }
    
    @Test
    public void removeAddOnForItem_Removed_ShouldReturnResponseStatusOK() throws Exception {
        mockMvc.perform(delete("/api/item/detail/{itemDetailId}", ID).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }
    
    @Test
    public void removeBasketItem_Removed_ShouldReturnResponseStatusOK() throws Exception {
        mockMvc.perform(delete("/api/item/{itemId}", ID).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }
    
    @Test
    public void orderBasket_ordered_ShouldReturnResponseStatusOK() throws Exception {
        mockMvc.perform(post("/api/item/order").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }
}
