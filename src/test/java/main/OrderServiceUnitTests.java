package main;

import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import tr.com.shop.coffee.exception.OrderNotFoundException;
import tr.com.shop.coffee.models.Order;
import tr.com.shop.coffee.repository.OrderRepository;
import tr.com.shop.coffee.service.OrderService;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceUnitTests {

    private OrderService service;
    
    @Mock
    private OrderRepository repository;
    
    private static final Long ID = 1L;
    
    @Before
    public void setup() {
        this.service = new OrderService(repository);
    }
    
    @Test(expected = OrderNotFoundException.class)
    public void getOrderById_NotFound_ShouldThrowException() {
        when(repository.findOneById(ID)).thenReturn(Optional.ofNullable(null));
        service.getOrderById(ID);
    }
    
    @Test
    public void getOrderById_Found_ShouldReturnTheFound() {
        Order order = new Order();
        order.setId(ID);
        when(repository.findOneById(ID)).thenReturn(Optional.of(order));
        Order found = service.getOrderById(ID);
        Assert.assertTrue(order.getId() == found.getId());
    }
    
}
