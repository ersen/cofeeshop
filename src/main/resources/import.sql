INSERT INTO products (id, name, price, type) VALUES (1, 'Filtre Kahve', 4, 'DRINK');
INSERT INTO products (id, name, price, type) VALUES (2, 'Mocha', 6, 'DRINK');
INSERT INTO products (id, name, price, type) VALUES (3, 'Cay', 3, 'DRINK');
INSERT INTO products (id, name, price, type) VALUES (4, 'Latte', 5, 'DRINK');
INSERT INTO products (id, name, price, type) VALUES (5, 'Sut', 2, 'ADDON');
INSERT INTO products (id, name, price, type) VALUES (6, 'Findik Surubu', 3, 'ADDON');
INSERT INTO products (id, name, price, type) VALUES (7, 'Cikolata Sosu', 5, 'ADDON');