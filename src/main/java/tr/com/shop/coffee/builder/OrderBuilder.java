package tr.com.shop.coffee.builder;

import tr.com.shop.coffee.models.Order;

public class OrderBuilder {

    private Order order;
    
    public OrderBuilder() {
        order = new Order();
    }
    
    public OrderBuilder id(long id) {
        order.setId(id);
        return this;
    }
    
    public Order get() {
        return order;
    }
}
