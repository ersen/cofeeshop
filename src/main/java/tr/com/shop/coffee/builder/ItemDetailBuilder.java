package tr.com.shop.coffee.builder;

import tr.com.shop.coffee.models.ItemDetail;

public class ItemDetailBuilder {

    public ItemDetail detail;
    
    public ItemDetailBuilder () {
        detail = new ItemDetail();
    }
    
    public ItemDetailBuilder id(Long id) {
        detail.setId(id);
        return this;
    }
    
    public ItemDetail get() {
        return detail;
    }
}
