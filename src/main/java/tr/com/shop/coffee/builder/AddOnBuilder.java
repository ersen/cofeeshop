package tr.com.shop.coffee.builder;

import tr.com.shop.coffee.models.domain.AddOn;

public class AddOnBuilder {

    AddOn addon;
    
    public AddOnBuilder() {
        addon = new AddOn();
    }
    
    public AddOnBuilder id(Long id) {
        addon.setId(id);
        return this;
    }
    
    public AddOnBuilder name(String name) {
        addon.setName(name);
        return this;
    }
    
    public AddOnBuilder price(int price) {
        addon.setPrice(price);
        return this;
    }
    
    public AddOn get() {
        return addon;
    }
   
}
