package tr.com.shop.coffee.builder;

import tr.com.shop.coffee.models.domain.Drink;

public class DrinkBuilder {

    Drink drink;
    
    public DrinkBuilder() {
        drink = new Drink();
    }
    
    public DrinkBuilder id(Long id) {
        drink.setId(id);
        return this;
    }
    
    public DrinkBuilder name(String name) {
        drink.setName(name);
        return this;
    }
    
    public DrinkBuilder price(int price) {
        drink.setPrice(price);
        return this;
    }
    
    public Drink get() {
        return drink;
    }
   
}
