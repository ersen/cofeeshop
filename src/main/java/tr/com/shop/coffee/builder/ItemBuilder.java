package tr.com.shop.coffee.builder;

import tr.com.shop.coffee.models.Item;
import tr.com.shop.coffee.models.Order;
import tr.com.shop.coffee.models.domain.Drink;

public class ItemBuilder {
    public Item item;
    
    public ItemBuilder () {
        item = new Item();
    }
    
    public ItemBuilder id(Long id) {
        item.setId(id);
        return this;
    }
    
    public ItemBuilder drink(Drink drink ) {
        item.setDrink(drink);
        return this;
    }
    
    public ItemBuilder order(Order order ) {
        item.setOrder(order);
        return this;
    }
    
    public Item get() {
        return item;
    }
}
