package tr.com.shop.coffee.controller.api;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tr.com.shop.coffee.dto.response.ProductDTO;
import tr.com.shop.coffee.enums.ProductType;
import tr.com.shop.coffee.models.Product;
import tr.com.shop.coffee.models.domain.Drink;
import tr.com.shop.coffee.service.ItemService;
import tr.com.shop.coffee.service.ProductService;

@RestController
@RequestMapping(value = "/api/product", produces = "application/json")
@Transactional
public class ProductController {

    private ProductService service;
    private ItemService itemService;

    @Autowired
    public ProductController(ProductService service, ItemService itemService) {
        this.service = service;
        this.itemService = itemService;
    }

    @RequestMapping(value = "/{type}", method = RequestMethod.GET)
    public List<ProductDTO> listByType(@PathVariable("type") ProductType type) {
        List<Product> products = service.listProductsByType(type);
        return products.stream().map(p -> new ProductDTO(p)).collect(Collectors.toList());
    }
    
    @RequestMapping(value = "/{productId}/basket", method = RequestMethod.POST)
    public void addToBasket(@PathVariable("productId") long productId) {
        Drink drink = service.getDrinkById(productId);
        itemService.addToBasket(drink);
    }
    
    
}
