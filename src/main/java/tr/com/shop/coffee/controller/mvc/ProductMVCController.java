package tr.com.shop.coffee.controller.mvc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import tr.com.shop.coffee.dto.response.ProductDTO;
import tr.com.shop.coffee.enums.ProductType;
import tr.com.shop.coffee.service.ClientService;

@Controller
public class ProductMVCController {

    @Autowired
    private ClientService client;
    
    @RequestMapping(value = "/drinks", method = RequestMethod.GET)
    public ModelAndView list() {
        List<ProductDTO> drinks = client.getProductsByType(ProductType.DRINK);
        
        ModelAndView mav = new ModelAndView();
        mav.addObject("drinks", drinks).setViewName("drinks");
        return mav;
    }
    
    @RequestMapping(value = "/{id}/basket", method = RequestMethod.POST)
    public String addToBasket(@PathVariable("id") Long id, final RedirectAttributes redirectAttributes) {
    	
        client.addDrinkToBasket(id);
    	
    	redirectAttributes.addFlashAttribute("msg", "Sepete eklendi!");
        
        return "redirect:/drinks";
    }
}
