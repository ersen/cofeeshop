package tr.com.shop.coffee.controller.mvc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import tr.com.shop.coffee.dto.response.ItemDTO;
import tr.com.shop.coffee.dto.response.OrderDTO;
import tr.com.shop.coffee.service.ClientService;

@Controller
public class OrderMVCController {

    @Autowired
    private ClientService client;
    
    @RequestMapping(value = "/order", method = RequestMethod.GET)
    public ModelAndView list() {   
        List<OrderDTO> items = client.getOrders();
 
        ModelAndView mav = new ModelAndView();
        mav.addObject("orders", items).setViewName("order");
        
        return mav;
    }
    
    @RequestMapping(value = "/order/{id}", method = RequestMethod.GET)
    public ModelAndView getOrderedItems(@PathVariable("id") long id) {   
        List<ItemDTO> items = client.getOrderedItems(id);
 
        ModelAndView mav = new ModelAndView();
        mav.addObject("items", items).setViewName("orderDetails");
        
        return mav;
    }
}
