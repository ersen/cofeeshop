package tr.com.shop.coffee.controller.api;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tr.com.shop.coffee.dto.response.ItemDetailDTO;
import tr.com.shop.coffee.dto.response.ItemDTO;
import tr.com.shop.coffee.models.Item;
import tr.com.shop.coffee.models.domain.AddOn;
import tr.com.shop.coffee.service.ItemService;
import tr.com.shop.coffee.service.ProductService;

@RestController
@RequestMapping(value = "/api/item", produces = "application/json")
@Transactional
public class ItemController {

    private ItemService service;
    private ProductService productService;

    @Autowired
    public ItemController(ItemService service, ProductService productService) {
        this.service = service;
        this.productService = productService;
    }

    @RequestMapping(value = "/unordered", method = RequestMethod.GET)
    public List<ItemDTO> list() {
        List<Item> items = service.getNonOrderedBasketItems();
        return items.stream().map(i -> new ItemDTO(i)).collect(Collectors.toList());
    }

    @RequestMapping(value = "/{itemId}/addons", method = RequestMethod.GET)
    public List<ItemDetailDTO> listItemAddons(@PathVariable("itemId") long itemId) {
        Item item = service.getItemById(itemId);
        return item.getItemDetails().stream().map(bao -> new ItemDetailDTO(bao)).collect(Collectors.toList());
    }

    @RequestMapping(value = "/{itemId}/detail/{addOnId}", method = RequestMethod.POST)
    public void addAddOnForItem(@PathVariable("itemId") long itemId, @PathVariable("addOnId") long addOnId) {
        Item item = service.getItemById(itemId);
        AddOn addOn = productService.getAddOnById(addOnId);
        service.addAddOnForItem(item, addOn);
    }

    @RequestMapping(value = "/detail/{itemDetailId}", method = RequestMethod.DELETE)
    public void removeAddOnForItem(@PathVariable("itemDetailId") long itemDetailId) {
        service.removeAddOnFromItem(itemDetailId);
    }

    @RequestMapping(value = "/{itemId}", method = RequestMethod.DELETE)
    public void removeBasketItem(@PathVariable("itemId") long itemId) {
        service.removeBasketItem(itemId);
    }

    @RequestMapping(value = "/order", method = RequestMethod.POST)
    public void orderBasket() {
        service.orderBasket();
    }
}
