package tr.com.shop.coffee.controller.mvc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import tr.com.shop.coffee.dto.response.ItemDetailDTO;
import tr.com.shop.coffee.dto.response.ProductDTO;
import tr.com.shop.coffee.enums.ProductType;
import tr.com.shop.coffee.service.ClientService;

@Controller
public class ItemMVController {

    @Autowired
    private ClientService client;
    
	@RequestMapping(value = "/item/{id}", method = RequestMethod.GET)
    public ModelAndView listItemDetails(@PathVariable("id") long itemId) {
           
        List<ProductDTO> addOns = client.getProductsByType(ProductType.ADDON);
 
        List<ItemDetailDTO> itemAddOns = client.getItemDetails(itemId);
        
        ModelAndView mav = new ModelAndView();
        mav.addObject("itemAddOns", itemAddOns).addObject("addOns", addOns).addObject("itemId", itemId).setViewName("itemDetails");
        
        return mav;
    }
	
	@RequestMapping(value = "/item/{itemId}/add/{addOnId}", method = RequestMethod.POST)
    public String addAddOn(@PathVariable("itemId") long itemId, @PathVariable("addOnId") long addOnId) {
        client.addItemDetail(itemId, addOnId);
        return "redirect:/item/" + itemId;
    }
	
	@RequestMapping(value = "/{itemId}/itemAddOn/{itemDetailId}/delete/", method = RequestMethod.POST)
    public String removeAddOn(@PathVariable("itemId") long itemId, @PathVariable("itemDetailId") long itemDetailId) {
        client.removeAddOn(itemDetailId);
        return "redirect:/item/" + itemId;
    }
	
	@RequestMapping(value = "/item/{itemId}/delete/", method = RequestMethod.POST)
    public String removeItem(@PathVariable("itemId") long itemId) {
        client.removeItem(itemId);
        return "redirect:/basket";
    }
	
	
}
