package tr.com.shop.coffee.controller.mvc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import tr.com.shop.coffee.dto.response.ItemDTO;
import tr.com.shop.coffee.service.ClientService;

@Controller
public class BasketMVCController {

    @Autowired
    private ClientService client;
    
	@RequestMapping(value = "/basket", method = RequestMethod.GET)
    public ModelAndView list() {   
        List<ItemDTO> items = client.getNonOrderedItems();
 
        ModelAndView mav = new ModelAndView();
        mav.addObject("items", items).setViewName("basket");
        
        return mav;
    }
	
	@RequestMapping(value = "/order", method = RequestMethod.POST)
    public String order() {
        client.orderItems();
        
        return "redirect:/basket";
    }
	
}
