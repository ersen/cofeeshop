package tr.com.shop.coffee.controller.api;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tr.com.shop.coffee.dto.response.ItemDTO;
import tr.com.shop.coffee.dto.response.OrderDTO;
import tr.com.shop.coffee.models.Order;
import tr.com.shop.coffee.service.OrderService;

@RestController
@RequestMapping(value = "/api/order", produces = "application/json")
@Transactional
public class OrderController {

    private OrderService service;
   
    @Autowired
    public OrderController(OrderService service) {
        this.service = service;
    }
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<OrderDTO> list() {
        List<Order> orders = service.listOrders();
        return orders.stream().map(o -> new OrderDTO(o)).collect(Collectors.toList());
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public List<ItemDTO> getOrderItems(@PathVariable("id") long id) {
        Order order = service.getOrderById(id);
        return order.getItems().stream().map(i -> new ItemDTO(i)).collect(Collectors.toList());
    }
}
