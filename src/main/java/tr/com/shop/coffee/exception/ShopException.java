package tr.com.shop.coffee.exception;

import tr.com.shop.coffee.enums.ErrorCodes;

public class ShopException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private int code;
    private String exceptionMessage;

    public ShopException(ErrorCodes code) {
        this.code = code.getCode();
    }

    public ShopException(ErrorCodes code, String message) {
        this(code);
        this.exceptionMessage = message;
    }

    public int getCode() {
        return code;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }
}
