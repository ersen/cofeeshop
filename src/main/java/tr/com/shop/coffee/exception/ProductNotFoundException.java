package tr.com.shop.coffee.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import tr.com.shop.coffee.enums.ErrorCodes;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such product")
public class ProductNotFoundException extends ShopException {

    private static final long serialVersionUID = 1L;

    public ProductNotFoundException() {
        super(ErrorCodes.ERR_PRODUCT_NOT_FOUND);
    }
}
