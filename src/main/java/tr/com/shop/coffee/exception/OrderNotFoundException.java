package tr.com.shop.coffee.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import tr.com.shop.coffee.enums.ErrorCodes;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such order")
public class OrderNotFoundException extends ShopException {

    private static final long serialVersionUID = 1L;

    public OrderNotFoundException() {
        super(ErrorCodes.ERR_ORDER_NOT_FOUND);
    }

}
