package tr.com.shop.coffee.repository;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;

import tr.com.shop.coffee.models.ItemDetail;

public interface ItemDetailRepository extends PagingAndSortingRepository<ItemDetail, Long>{

	Optional<ItemDetail> findOneById(long id);
}
