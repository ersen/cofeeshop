package tr.com.shop.coffee.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import tr.com.shop.coffee.models.Item;

public interface ItemRepository extends PagingAndSortingRepository<Item, Long>{

	@Query("SELECT b FROM Item b WHERE b.order IS NULL")
	List<Item> listNonOrderedItems();
	
	Optional<Item> findOneById(long id);
}
