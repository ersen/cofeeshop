package tr.com.shop.coffee.repository;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;

import tr.com.shop.coffee.models.Order;

public interface OrderRepository extends PagingAndSortingRepository<Order, Long>{

    Optional<Order> findOneById(long id);
}
