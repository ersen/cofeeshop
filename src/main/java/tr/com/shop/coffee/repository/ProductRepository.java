package tr.com.shop.coffee.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;

import tr.com.shop.coffee.enums.ProductType;
import tr.com.shop.coffee.models.Product;

public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {

    List<Product> findByType(ProductType type);
    
    Optional<Product> findOneById(long id);
}
