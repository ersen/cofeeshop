package tr.com.shop.coffee.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tr.com.shop.coffee.exception.OrderNotFoundException;
import tr.com.shop.coffee.models.Order;
import tr.com.shop.coffee.repository.OrderRepository;

@Service
public class OrderService {

    private OrderRepository repository;

    @Autowired
    public OrderService(OrderRepository repository) {
        this.repository = repository;
    }
    /**
     * Lists all orders
     * 
     * @return list of {@link Order}
     */
    public List<Order> listOrders() {
        Iterable<Order> it = repository.findAll();
        Iterator<Order> itr = it.iterator();
        
        List<Order> list = new ArrayList<Order>();
        while(itr.hasNext()) {
          Order obj = (Order)itr.next();
          list.add(obj);
        }
        
        return list;
    }

    /**
     * Returns an {@link Order} by specified id
     * 
     * @param id
     * @return the {@link Order}
     * 
     * @throws OrderNotFoundException in case of passing non-existent id 
     */
    public Order getOrderById(long id) {
        return repository.findOneById(id).orElseThrow(() -> new OrderNotFoundException());
    }

}
