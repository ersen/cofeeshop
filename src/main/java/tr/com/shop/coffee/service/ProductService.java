package tr.com.shop.coffee.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tr.com.shop.coffee.enums.ProductType;
import tr.com.shop.coffee.exception.InvalidRequestParametersException;
import tr.com.shop.coffee.exception.ProductNotFoundException;
import tr.com.shop.coffee.models.Product;
import tr.com.shop.coffee.models.domain.AddOn;
import tr.com.shop.coffee.models.domain.Drink;
import tr.com.shop.coffee.repository.ProductRepository;

@Service
public class ProductService {

    private ProductRepository repository;
    
    @Autowired
    public ProductService(ProductRepository repository) {
        this.repository = repository;
    }
    
    /**
     * List all {@link Product} by specified {@link ProductType}
     * 
     * @param type the {@link ProductType}
     * 
     * @return list of {@link Product}
     * 
     */
    public List<Product> listProductsByType(ProductType type) {
        return repository.findByType(type);
    }
    
    /**
     * gets an {@link AddOn} by specified id
     *  
     * @param id 
     * @return {@link AddOn}
     * 
     * @see ProductService#getProductById(long)
     * 
     * @throws InvalidRequestParametersException in case of founding type is not a {@link ProductType#DRINK} 
     */
    public Drink getDrinkById(long id) {
    	Product product = getProductById(id);
    	
    	if (!product.isDrink())
    	    throw new InvalidRequestParametersException("passing id is not a drink");
    	
    	return (Drink)product;
    	
    }
    
    /**
     * gets an {@link AddOn} by specified id
     *  
     * @param id 
     * @return {@link AddOn}
     * 
     * @see ProductService#getProductById(long)
     * 
     * @throws InvalidRequestParametersException in case of founding type is not an {@link ProductType#ADDON} 
     */
    public AddOn getAddOnById(long id) {
    	Product product = getProductById(id);
    	
    	if (!product.isAddon())
    		throw new InvalidRequestParametersException("passing id is not an addon");
    	
    	return (AddOn)product;
    	
    }
    
    /**
     * gets the {@link Product} by specified id
     * 
     * @param id the specified id
     * 
     * @return the {@link Product}
     * 
     * @throws ProductNotFoundException in case of passing a non-existent id
     */
    public Product getProductById(long id) {
    	return repository.findOneById(id).orElseThrow(() -> new ProductNotFoundException());
    }

}
