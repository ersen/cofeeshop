package tr.com.shop.coffee.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import tr.com.shop.coffee.dto.response.ItemDTO;
import tr.com.shop.coffee.dto.response.ItemDetailDTO;
import tr.com.shop.coffee.dto.response.OrderDTO;
import tr.com.shop.coffee.dto.response.ProductDTO;
import tr.com.shop.coffee.enums.ProductType;

@Service
public class ClientService {

    private static final String PRODUCT_URI = "/api/product";
    
    private static final String ITEM_URI = "/api/item";
    
    private static final String ORDER_URI = "/api/order";
    
    @Value("${server.host:localhost}")
    private String serverHost;
    
    @Value("${server.port}")
    private int serverPort;
    
    private String getServerURI() {
        return "http://" + serverHost + ":" + serverPort;
    }
    
    @SuppressWarnings("unchecked")
    public List<ProductDTO> getProductsByType(ProductType type) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("type", type.toString());
        
        RestTemplate restTemplate = new RestTemplate();
        String url = getServerURI() + PRODUCT_URI + "/{type}";   
        return restTemplate.getForObject(url, List.class, params);
    }
    
    public void addDrinkToBasket (Long id) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("productId", String.valueOf(id));
        
        RestTemplate restTemplate = new RestTemplate();
        String url = getServerURI() + PRODUCT_URI + "/{productId}/basket";   
        restTemplate.postForObject(url, null, Void.class, params);
    }
    
    @SuppressWarnings("unchecked")
    public List<ItemDetailDTO> getItemDetails(Long itemId) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("itemId", String.valueOf(itemId));
        
        RestTemplate restTemplate = new RestTemplate();
        String url = getServerURI() + ITEM_URI + "/{itemId}/addons";   
        return restTemplate.getForObject(url, List.class, params);
    }
    
    public void addItemDetail(long itemId, long addOnId) {
        RestTemplate restTemplate = new RestTemplate();
        
        Map<String, String> params = new HashMap<String, String>();
        params = new HashMap<String, String>();
        params.put("itemId", String.valueOf(itemId));
        params.put("addOnId", String.valueOf(addOnId));
        
        String url = getServerURI() + ITEM_URI + "/{itemId}/detail/{addOnId}";   
        restTemplate.postForObject(url, null, Void.class, params);
    }
    
    public void removeAddOn(long itemDetailId) {
        RestTemplate restTemplate = new RestTemplate();
        
        Map<String, String> params = new HashMap<String, String>();
        params = new HashMap<String, String>();
        params.put("itemDetailId", String.valueOf(itemDetailId));
        
        String url = getServerURI() + ITEM_URI + "/detail/{itemDetailId}";   
        restTemplate.delete(url, params);
    }
    
    public void removeItem(long itemId) {
        RestTemplate restTemplate = new RestTemplate();
        
        Map<String, String> params = new HashMap<String, String>();
        params = new HashMap<String, String>();
        params.put("itemId", String.valueOf(itemId));
        
        String url = getServerURI() + ITEM_URI + "/{itemId}";   
        restTemplate.delete(url, params);
    }
    
    @SuppressWarnings("unchecked")
    public List<ItemDTO> getNonOrderedItems() {
        RestTemplate restTemplate = new RestTemplate();
        String url = getServerURI() + ITEM_URI + "/unordered";   
        return restTemplate.getForObject(url, List.class);
    }
    
    public void orderItems() {
        RestTemplate restTemplate = new RestTemplate();
        String url = getServerURI() + ITEM_URI + "/order";   
        restTemplate.postForObject(url, null, Void.class);
    }
    
    @SuppressWarnings("unchecked")
    public List<ItemDTO> getOrderedItems(long orderId) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("id", String.valueOf(orderId));
        
        RestTemplate restTemplate = new RestTemplate();
        String url = getServerURI() + ORDER_URI + "/{id}";   
        return restTemplate.getForObject(url, List.class, params);
    }
    
    @SuppressWarnings("unchecked")
    public List<OrderDTO> getOrders() {
        RestTemplate restTemplate = new RestTemplate();
        String url = getServerURI() + ORDER_URI + "/";   
        return restTemplate.getForObject(url, List.class);
    }
    
}
