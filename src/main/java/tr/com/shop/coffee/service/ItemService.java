package tr.com.shop.coffee.service;

import java.util.List;

import javax.xml.soap.Detail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tr.com.shop.coffee.models.ItemDetail;
import tr.com.shop.coffee.exception.InvalidRequestParametersException;
import tr.com.shop.coffee.exception.ItemDetailNotFoundException;
import tr.com.shop.coffee.exception.ItemNotFoundException;
import tr.com.shop.coffee.models.Item;
import tr.com.shop.coffee.models.Order;
import tr.com.shop.coffee.models.domain.AddOn;
import tr.com.shop.coffee.models.domain.Drink;
import tr.com.shop.coffee.repository.ItemDetailRepository;
import tr.com.shop.coffee.repository.ItemRepository;
import tr.com.shop.coffee.repository.OrderRepository;

@Service
public class ItemService {

	private ItemRepository repository;
	
	private ItemDetailRepository itemDetailRepository;
	
	private OrderRepository orderRepository;
	
	@Autowired
	public ItemService(ItemRepository repository, ItemDetailRepository itemDetailRepository, OrderRepository orderRepository) {
		this.repository = repository;
		this.itemDetailRepository = itemDetailRepository;
		this.orderRepository = orderRepository;
	}
	
	/**
	 * Adds a {@link Drink} in a basket
	 * 
	 * @param drink the {@link Drink}
	 * @return
	 */
    public Item addToBasket(Drink drink) {
        Item item = new Item();
        item.setDrink(drink);
        return repository.save(item);
    }

    /**
     * Lists the items which doesn't ordered yet
     * 
     * @return List of {@link Item}
     */
	public List<Item> getNonOrderedBasketItems() {
		return repository.listNonOrderedItems();
	}

	/**
	 * Gets an {@link Item} specified by given id
	 * 
	 * @param itemId the id of {@link Item}
	 * @return {@link Item}
	 * 
	 * @throws ItemNotFoundException in case of passing nonexistent id
	 */
	public Item getItemById(long itemId) {
		return repository.findOneById(itemId).orElseThrow(() -> new ItemNotFoundException());
	}
	
	/**
	 * Adds an {@link AddOn} for an {@link Item} in the basket. (i.e not ordered yet)
	 * 
	 * @param item the specified {@link Item}
	 * @param addOn the specified {@link AddOn}
	 * @return newly created {@link ItemDetail}
	 * 
	 * @throws InvalidRequestParametersException in case of passing ordered {@link Item}
	 */
	public ItemDetail addAddOnForItem(Item item, AddOn addOn) {
	    if (item.getOrder().isPresent())
	        throw new InvalidRequestParametersException("item ordered already");
	    
		ItemDetail detail = new ItemDetail();
		detail.setAddon(addOn);
		detail.setItem(item);
		return itemDetailRepository.save(detail);
	}

	/**
	 * Removes {@link AddOn} from specified {@link ItemDetail}
	 * 
	 * @param itemDetailId
	 * 
	 * @throws ItemDetailNotFoundException in case of passing non-existent {@link Detail} id
	 */
	public void removeAddOnFromItem(long itemDetailId) {
		ItemDetail itemDetail = itemDetailRepository.findOneById(itemDetailId).orElseThrow(() -> new ItemDetailNotFoundException());
		itemDetailRepository.delete(itemDetail);
	}

	/**
	 * Deletes the specified itemId
	 * @param itemId
	 * 
	 * @see ItemService#getItemById(long)
	 */
	public void removeBasketItem(long itemId) {
		Item item = getItemById(itemId);
		repository.delete(item);		
	}

	/**
	 * Order all non-ordered items
	 * @return
	 */
    public Order orderBasket() {
        List<Item> items = getNonOrderedBasketItems();
        final Order order = new Order();
        order.setItems(items);
        items.forEach(i -> i.setOrder(order));
        return orderRepository.save(order);
    }

}
