package tr.com.shop.coffee.dto.response;

import tr.com.shop.coffee.models.ItemDetail;
import tr.com.shop.coffee.models.domain.AddOn;

public class ItemDetailDTO {

    public long id;
    public AddOn addon;
    
    public ItemDetailDTO(ItemDetail itemAddOn) {
        this.id = itemAddOn.getId();
        this.addon = itemAddOn.getAddon();
    }
}
