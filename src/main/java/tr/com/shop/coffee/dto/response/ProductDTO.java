package tr.com.shop.coffee.dto.response;

import tr.com.shop.coffee.models.Product;

public class ProductDTO {

    public long id;
    
    public String name;
    
    public int price;
    
    public ProductDTO(Product product) {
        this.id = product.getId();
        this.name = product.getName();
        this.price = product.getPrice();
    }
}
