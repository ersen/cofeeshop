package tr.com.shop.coffee.dto.response;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import tr.com.shop.coffee.models.Order;

public class OrderDTO {

    public long id;
    public String createdDate;
    
    public OrderDTO (Order order) {
        this.id = order.getId();
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        this.createdDate = formatter.format(order.getCreatedDate());
    }
}
