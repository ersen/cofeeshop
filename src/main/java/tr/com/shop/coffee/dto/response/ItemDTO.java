package tr.com.shop.coffee.dto.response;

import java.util.List;
import java.util.stream.Collectors;
import tr.com.shop.coffee.models.Item;
import tr.com.shop.coffee.models.domain.AddOn;

public class ItemDTO {

	public long id;
	public String drinkName;
	public int price;
	public List<AddOn> addOns;
	
	public ItemDTO(Item item) {
		this.id = item.getId();
		this.drinkName = item.getDrink().getName();
		this.price = item.getTotalPrice();
		this.addOns = item.getItemDetails().stream().map(ia -> ia.getAddon()).collect(Collectors.toList());
	}
	
}
