package tr.com.shop.coffee.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import tr.com.shop.coffee.models.domain.Drink;

@Entity
@Table(name = "items")
public class Item {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @ManyToOne
    @JoinColumn(name = "drink_id")
    private Drink drink;
    
    @ManyToOne
    @JoinColumn(name = "order_id")
    private Order order;
    
    @OneToMany(mappedBy = "item", cascade = CascadeType.ALL)
    private List<ItemDetail> itemDetails = new ArrayList<ItemDetail>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Drink getDrink() {
		return drink;
	}

	public void setDrink(Drink drink) {
		this.drink = drink;
	}

	public Optional<Order> getOrder() {
		return Optional.ofNullable(order);
	}

	public void setOrder(Order order) {
		this.order = order;
	}
	
    public List<ItemDetail> getItemDetails() {
        return itemDetails;
    }

    public void setItemDetails(List<ItemDetail> itemDetails) {
        this.itemDetails = itemDetails;
    }

    public int getTotalPrice() {
		int drinkPrice = getDrink().getPrice();
		int addOnPrice = getItemDetails().stream().mapToInt(ia -> ia.getAddon().getPrice()).sum();
		return drinkPrice + addOnPrice;
	}

	
    
}
