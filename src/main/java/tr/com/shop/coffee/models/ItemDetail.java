package tr.com.shop.coffee.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tr.com.shop.coffee.models.domain.AddOn;

@Entity
@Table(name = "item_details")
public class ItemDetail {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
	
	@ManyToOne
	@JoinColumn(name = "addon_id")
	private AddOn addon;
	
	@ManyToOne
	@JoinColumn(name = "item_id")
	private Item item;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AddOn getAddon() {
		return addon;
	}

	public void setAddon(AddOn addon) {
		this.addon = addon;
	}

	public Item getItem() {
		return item;
	}
	
	public void setItem(Item item) {
		this.item = item;
	}
}
