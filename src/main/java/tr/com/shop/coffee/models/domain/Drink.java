package tr.com.shop.coffee.models.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import tr.com.shop.coffee.enums.ProductType;
import tr.com.shop.coffee.models.Product;

@Entity
@DiscriminatorValue(value = "DRINK")
public class Drink extends Product {

    public Drink() {
        setType(ProductType.DRINK);
    }
}
