package tr.com.shop.coffee.models.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import tr.com.shop.coffee.enums.ProductType;
import tr.com.shop.coffee.models.Product;

@Entity
@DiscriminatorValue(value = "ADDON")
public class AddOn extends Product {

    public AddOn() {
        setType(ProductType.ADDON);
    }
}
