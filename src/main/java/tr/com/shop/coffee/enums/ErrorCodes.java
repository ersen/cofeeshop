package tr.com.shop.coffee.enums;


public enum ErrorCodes {

    INTERNAL_SERVER_ERROR(1000),
    ERR_ITEM_NOT_FOUND(1001),
    ERR_ITEM_DETAIL_FOUND(1002), 
    ERR_PRODUCT_NOT_FOUND(1003),
    ERR_ORDER_NOT_FOUND(1004),
    ERR_INVALID_REQUEST_PARAMETERS(1020);
    
    private int code;

    ErrorCodes(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}