package tr.com.shop.coffee.enums;

public enum ProductType {
    DRINK,
    ADDON;
}
